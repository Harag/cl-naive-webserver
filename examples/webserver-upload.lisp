(in-package :cl-user)

(defvar *port* 3333)

(defpackage :naive-examples
  (:use :cl :cl-naive-webserver))
(in-package :naive-examples)


(defparameter *server*
  (make-instance 'cl-naive-webserver.hunchentoot:hunchentoot-server
		 :address "localhost"
		 :port cl-user::*port*))
(start-server *server*)

(defparameter *simple-site*
  (make-instance 'site :url "/simple"))
(register-site *simple-site*)


(setf (find-resource (handlers *simple-site*) "/example")
      (lambda (script-name)
	(declare (ignore script-name))
	(cl-naive-who-ext:with-html-to-string
	  "Yeeeha she is a live and kicking!!")))

(setf (find-resource (handlers *simple-site*) "/get-headers")
      (lambda (script-name)
	(declare (ignore script-name))
        (push (cons :content-type "text/plain") (headers *reply*))
        (push (cons :x-test "Hello, World!") (headers *reply*))
        "Yeeeha she is a live and kicking!!"))

(assert (string= (cdr (assoc :x-test (nth-value 2 (drakma:http-request
                                                   "http://localhost:3333/simple/get-headers"
                                                   :method :get))))
                 "Hello, World!"))


(assert (string= (drakma:http-request
                  "http://localhost:3333/simple/example"
                  :method :get)
                 "Yeeeha she is a live and kicking!!"))

;;Try the following in your browser it should give you an authorization error

;;http://localhost:3333/simple/example

;;Get a token issued first

;;http://localhost:3333/simple/token

;;No try /example again

;;http://localhost:3333/simple/example




(in-package :naive-examples)

;; 1- since we are using cl-naive-webserver we need to have a /site/resource path.
;; 2- the site must exist.
;; 3- a handler must exist.

(defvar *upload-directory* "/tmp/")

(setf (find-resource (handlers *simple-site*) "/file-csv-upload")
      (lambda (script-name)
	(declare (ignore script-name))
        (with-open-file (file (merge-pathnames "uploaded" *upload-directory*)
                              :direction :output
                              :if-exists :supersede
                              :if-does-not-exist :create)
          (write-string (hunchentoot:raw-post-data :force-text t) file)
          (finish-output file)
          (format nil "Got ~A characters." (file-length file)))))


(defun demo-upload-file ()
  ;; upload:
  (let ((contents "Hao Wang, logicien americain.

L'algorithme en  question  a  �t�  publi�  en  1960  dans l'IBM Journal,
article intitule \"Toward  Mechanical Mathematics\", avec des variantes et
une  extension au calcul  des  pr�dicats.  Il  s'agit  ici  du  \"premier
programme\" de Wang, syst�me \"P\".

L'article a �t� �crit en 1958, et les exp�riences effectu�es sur IBM 704
� machine � lampes, 32 k  mots  de 36 bits, celle�l� m�me qui vit na�tre
LISP � la m�me �poque. Le programme  a  �t� �crit en assembleur (Fortran
existait, mais il ne s'�tait pas encore impos�)  et  l'auteur estime que
\"there is very little in the program that is not straightforward\".

Il observe que les preuves engendr�es sont \"essentiellement des arbres\",
et  annonce  que  la  machine  a  d�montr� 220 th�or�mes du  calcul  des
propositions  (tautologies)  en  3  minutes. Il en tire argument pour la
sup�riorit�  d'une  approche  algorithmique  par  rapport � une approche
heuristique comme celle du \"Logic Theorist\" de Newell, Shaw et  Simon (�
partir de 1956 sur la machine JOHNNIAC de la Rand Corporation): un d�bat
qui dure encore...

Cet  algorithme  a  �t� popularis� par J. McCarthy, comme exemple�fanion
d'application  de LISP. Il figure dans le manuel de la premi�re  version
de  LISP  (LISP  1,  sur IBM 704 justement, le manuel est dat�  de  Mars
1960), et il a �t� repris dans le c�l�bre \"LISP 1.5 Programmer's Manual\"
publi� en 1962 par MIT Press, un des ma�tres�livres de l'Informatique.
"))
    (multiple-value-bind (body status-code headers uri http-stream must-close status-text)
        (drakma:http-request
         "http://localhost:3333/simple/file-csv-upload"
         :method :post
         :content-type "text/plain ; charset=UTF-8"
         :external-format-out :utf-8
         :external-format-in :utf-8
         :content (babel:string-to-octets contents :encoding :utf-8))

      (format t "~&BODY: ~S~%" body)
      (format t "~&STATUS: ~S ~S~%" status-code status-text)
      (format t "~&HEADERS: ~S~%" headers)
      (format t "~&URI: ~S~%" uri)
      (when must-close
        (close http-stream)))
    (assert (string= (uiop:run-program (format nil "cat ~A" (merge-pathnames "uploaded" *upload-directory*))
                       :output :string)
                     contents))))

(demo-upload-file)


