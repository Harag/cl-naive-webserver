(defsystem "webserver-upload"
  :description "An upload example."
  :version "2023.10.30"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-webserver.hunchentoot
               :cl-naive-who-ext
               :drakma)
  :components ((:file "examples/webserver-upload")))
