(in-package :cl-naive-webserver.woo)

(defclass woo-server (server)
  ())

(defclass woo-reply-wapper (reply)
  ((return-code :initarg :return-code
                :initform 200
                :accessor return-code
                :documentation "HTTP return code.")
   (headers :initarg :headers
            :initform nil
            :accessor headers
            :documentation "Out going headers.")
   (body :initarg :body
         :initform '("")
         :reader body
         :documentation "The body of the reply.")))

(defmethod make-reply ((server woo-server) raw-reply)
  (make-instance 'woo-reply-wrapper
                 :return-code (first raw-reply)
                 :headers (second raw-reply)
                 :body (third raw-reply)))

(defmethod set-cookie* ((reply reply) cookie)
  (setf (getx reply (cookie-name cookie) (stringify-cookie-value cookie))))

(defun query-string-params (query-string)
  (let ((query-string (split-sequence:split-sequence #\& query-string))
        (params))

    (dolist (param query-string)
      (when param
        (let ((pair (split-sequence:split-sequence #\= param)))
          (push pair params))))
    params))

(defun read-body (request)
  (let ((body
          (make-array (getf request :content-length)
                      :element-type '(unsigned-byte 8))))
    (read-sequence body (getf request :raw-body))
    body))

(defmethod make-request ((server woo-server) raw-request)
  (let ((body-octets (read-body raw-request))
        (body (and body-octets (babel:octets-to-string body-octets))))
    (make-instance 'request
                   :back-end-request raw-request
                   :headers (getx raw-request headers)
                   :script-name (getx raw-request script-name) ;;??? always ""
                   :server-protocol (getx raw-request server-protocol)
                   :remote-address (getx raw-request remote-uri)
                   :remote-port (getx raw-request remote-port)
                   :query-string (query-string-params (getx raw-request query-string))
                   :parameters (and body (read-from-string body)))))

(defmethod bad-request ((request woo:request) &key &allow-other-keys)
  (list 401
        (list :content-type "text/plain")
        (list (format nil "No handler for ~A" (getf request :REQUEST-URI)))))

(defmethod handle-request (server request &key &allow-other-keys)
  (let ((handler (find-resource *handlers*
                                (cdr (split-sequence:split-sequence
                                      #\/
                                      (getf request :path-info))))))
    (unless handler
      (return-from handle-request
        (bad-request)))

    (when handler)))

(defmethod start-server ((server woo-server) &key &allow-other-keys)
  (bt:make-thread
   (lambda ()
     (woo:run
      (lambda (env)
        (if (find script-name default-exclusions
                  :test (lambda (name exclusion)
                          (search exclusion name)))
            (let ((*request* (make-request server env))
                  (*reply*   (make-reply server (list 200
                                                      (list :content-type "text/plain")
                                                      (list "")))))
              (cond ((equalp script-name "/token")
                     (handle-token acceptor *request*
                                   (parameter "credentials")))
                    (*session*
                     (let ((response (handle-request listener env)))
                       (if response
                           (list (return-code *reply*)
                                 (headers *reply*)
                                 (list response))
                           (list 401
                                 (list :content-type "text/plain")
                                 (list "Resource not found")))))
                    (t
                     (list 401
                           (list :content-type "text/plain")
                           (list "No session."))))) 
            ;;TODO: Figure out what to pass back if nginx handled the request.
            ;;Should such a request even reach woo?
            (list 200
                  (list :content-type "text/plain")
                  nil)))

      :address *bound-ip*
      :port (port *listener*)))
   :named (format nil "Woo web server ~A" server)
   :initial-bindings (webserver-special-bindings)))
